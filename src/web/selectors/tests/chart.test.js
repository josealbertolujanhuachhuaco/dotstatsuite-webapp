import { getChart, getSharedChartData, getChartError, getStatus, getPublication, isDone } from '../chart';

const state = {
  chart: {
    sharedChart: {},
    hasError: false,
    status: 'publishing',
    publication: {},
  },
};

describe('selectors | chart', () => {
  it('getChart', () => {
    const { chart } = state;
    expect(getChart(state)).toEqual(chart);
  });

  it('getSharedChartData', () => {
    const {
      chart: { sharedChart },
    } = state;
    expect(getSharedChartData(state)).toEqual(sharedChart);
  });

  it('getChartError', () => {
    const {
      chart: { hasError },
    } = state;
    expect(getChartError(state)).toEqual(hasError);
  });

  it('getStatus', () => {
    const {
      chart: { status },
    } = state;
    expect(getStatus(state)).toEqual(status);
  });

  it('getPublication', () => {
    const {
      chart: { publication },
    } = state;
    expect(getPublication(state)).toEqual(publication);
  });

  it('isDone', () => {
    expect(isDone(state)).toEqual(true);
  });
});
