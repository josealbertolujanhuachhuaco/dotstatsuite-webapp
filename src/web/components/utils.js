import { compose, join, values, pick, map, take, split, toUpper } from 'ramda';

export const fullname = compose(
  join(' '),
  values,
  pick(['name']),
  x => x || {},
);

export const COLORS = ['red', 'purple', 'indigo', 'lightBlue', 'teal', 'lightGreen', 'orange'];
export const randomColor = () => COLORS[Math.round(Math.random() * (COLORS.length - 1))];
export const getColor = (obj = {}) => obj.color || randomColor();

export const initials = compose(
  join(''),
  map(take(1)),
  take(3),
  split(' '),
  toUpper,
);
