const makeTenant = id => `?tenant=${id}`;

export const getRedirectUri = (routePath, tenant) => `${window.location.origin}${routePath}${makeTenant(tenant)}`;
