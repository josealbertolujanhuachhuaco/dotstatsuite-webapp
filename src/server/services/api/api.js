import { getUserOnline, getUserOffline } from './utils';

const NAME = 'api';

export const service = {
  doit_offline() {
    return Promise.resolve(this.locals.req.user);
  },
  doit_online() {
    return Promise.resolve(this.locals.req.user);
  },
};

export default evtx => {
  evtx.use(NAME, service);
  evtx.service(NAME).before({
    doit_online: [getUserOnline()],
    doit_offline: [getUserOffline()],
  });
};
