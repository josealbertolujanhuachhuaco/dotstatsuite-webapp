import React from 'react';
import { shallow } from 'enzyme';
import PublicationForm from '..';

describe('PublicationForm', () => {
  it('should render', () => {
    const props = {
      onClick: jest.fn(),
      endPublication: jest.fn(),
      open: false,
      isDone: false,
      error: false,
    };
    const root = <PublicationForm {...props} />;
    const wrapped = shallow(root);
    expect(wrapped).toMatchSnapshot();
  });
});
