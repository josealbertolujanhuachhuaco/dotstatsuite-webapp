import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { Grid, Button, withStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import { compose, withState, withHandlers } from 'recompose';
import { Header, HeaderLeft } from '../../components/InnerHeader';
import { doitOffline, doitOnline } from '../../ducks/backend';

const styles = {
  header: {
    width: '100%',
  },
  text: {
    margin: '10px',
  },
};

const Private = ({ content, handleDoitOffline, handleDoitOnline, classes }) => (
  <Grid container spacing={16}>
    <Grid item className={classes.header}>
      <Header>
        <HeaderLeft>Private Page Sample</HeaderLeft>
      </Header>
    </Grid>
    <Grid item className={classes.text}>
      <p>
        In his tractibus navigerum nusquam visitur flumen sed in locis plurimis aquae suapte natura calentes emergunt ad
        usus aptae multiplicium medelarum. verum has quoque regiones pari sorte Pompeius Iudaeis domitis et Hierosolymis
        captis in provinciae speciem delata iuris dictione formavit.
      </p>
      <p>
        Nihil morati post haec militares avidi saepe turbarum adorti sunt Montium primum, qui divertebat in proximo,
        levi corpore senem atque morbosum, et hirsutis resticulis cruribus eius innexis divaricaturn sine spiramento
        ullo ad usque praetorium traxere praefecti.
      </p>
      <p>
        Et interdum acciderat, ut siquid in penetrali secreto nullo citerioris vitae ministro praesente paterfamilias
        uxori susurrasset in aurem, velut Amphiarao referente aut Marcio, quondam vatibus inclitis, postridie disceret
        imperator. ideoque etiam parietes arcanorum soli conscii timebantur.
      </p>
      <p>
        Exsistit autem hoc loco quaedam quaestio subdifficilis, num quando amici novi, digni amicitia, veteribus sint
        anteponendi, ut equis vetulis teneros anteponere solemus. Indigna homine dubitatio! Non enim debent esse
        amicitiarum sicut aliarum rerum satietates; veterrima quaeque, ut ea vina, quae vetustatem ferunt, esse debet
        suavissima; verumque illud est, quod dicitur, multos modios salis simul edendos esse, ut amicitiae munus
        expletum sit.
      </p>
    </Grid>
    <Grid container direction="column" spacing={8}>
      <Grid item>
        <Grid container spacing={16} justify="center">
          <Grid item>
            <Button onClick={handleDoitOffline} color="primary" size="small" variant="contained">
              Call backend and check offline
            </Button>
          </Grid>
          <Grid item>
            <Button onClick={handleDoitOnline} color="primary" size="small" variant="contained">
              Call backend and check online
            </Button>
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <pre className={classes.text}>{content}</pre>
      </Grid>
    </Grid>
  </Grid>
);

Private.propTypes = {
  doitOffline: PropTypes.func.isRequired,
  doitOnline: PropTypes.func.isRequired,
  content: PropTypes.string,
  setContent: PropTypes.func.isRequired,
  handleDoitOffline: PropTypes.func.isRequired,
  handleDoitOnline: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

const mapDispatchToProps = dispatch => bindActionCreators({ doitOffline, doitOnline }, dispatch);

const enhance = compose(
  connect(
    null,
    mapDispatchToProps,
  ),
  withState('content', 'setContent', null),
  withHandlers({
    handleDoitOffline: ({ doitOffline, setContent }) => () => {
      setContent('calling backend..');
      doitOffline()
        .then(content => setContent(`userinfo: ${JSON.stringify(content, null, 2)}`))
        .catch(() => setContent('error processing the request'));
    },
    handleDoitOnline: ({ doitOnline, setContent }) => () => {
      setContent('calling backend..');
      doitOnline()
        .then(content => setContent(`userinfo: ${JSON.stringify(content, null, 2)}`))
        .catch(() => setContent('error processing the request'));
    },
  }),
  withStyles(styles),
);

export default enhance(Private);
