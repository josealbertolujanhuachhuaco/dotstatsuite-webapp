import { createSelector } from 'reselect';
import { prop } from 'ramda';

export const getSettings = prop('settings');

export const getInitialChartData = createSelector(
  getSettings,
  settings => prop('intialData', settings),
);
