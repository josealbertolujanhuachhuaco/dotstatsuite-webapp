const proxy = {
  host: process.env.PROXY_HOST || '0.0.0.0',
  port: Number(process.env.PROXY_PORT) || 7000,
};

module.exports = {
  secretKey: process.env.SECRET_KEY,
  proxy,
};
