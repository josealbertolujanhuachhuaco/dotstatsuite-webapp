import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'components.Header.title',
    defaultMessage: 'WebApp',
  },
  public: {
    id: 'components.Header.public',
    defaultMessage: 'Publication',
  },
  private: {
    id: 'components.Header.private',
    defaultMessage: 'Private',
  },
});
