import axios from 'axios';
import jsdom from 'jsdom';
import initHttp from '../init/http';
import initRouter from '../init/router';
import initServices from '../services';
import initConfig from '../init/config';
import initAssets from '../init/assets';

const { JSDOM } = jsdom;
let CTX;

jest.mock('../configProvider', () => () => ({
  getI18n: () => Promise.resolve('I18N'),
  getTenant: () => Promise.resolve({ id: 'DEFAULT' }),
  getTenants: () => Promise.resolve({ id: 'DEFAULT' }),
  getSettings: () => Promise.resolve('SETTINGS'),
}));

describe('Main', function() {
  beforeAll(() => {
    return initConfig()
      .then(ctx => ({ ...ctx, config: { ...ctx.config, env: 'ENV' } }))
      .then(initAssets)
      .then(ctx => ({ ...ctx, assets: { ...ctx.assets, main: 'MAIN', vendors: 'VENDORS' } }))
      .then(initServices)
      .then(initRouter)
      .then(initHttp)
      .then(ctx => (CTX = ctx));
  });

  afterAll(() => CTX.httpServer.close());

  it('should render page', () => {
    const url = `${CTX.httpServer.url}`;
    return axios({ url }).then(({ data }) => {
      const dom = new JSDOM(data, { runScripts: 'dangerously' });
      expect(dom.window.I18N).toEqual('I18N');
      expect(dom.window.SETTINGS).toEqual('SETTINGS');
      expect(dom.window.CONFIG.tenant.id).toEqual('DEFAULT');
      expect(dom.window.CONFIG.env).toEqual('ENV');
      expect(dom.window.document.querySelector('script[src=VENDORS]')).toBeDefined();
      expect(dom.window.document.querySelector('script[src=MAIN]')).toBeDefined();
    });
  });
});
