import { combineReducers } from 'redux';
import chart from './chart';
import core from './core';
import auth from './auth';
import config from './config';
import settings from './settings';

const root = combineReducers({
  auth,
  chart,
  core,
  config,
  settings,
});

export default root;
