import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core';

const FinalTextField = ({ input: { name, onChange, value }, meta, ...rest }) => (
  <div>
    <TextField
      {...rest}
      autoFocus
      fullWidth
      name={name}
      type="text"
      value={value}
      onChange={onChange}
      error={meta.error && meta.touched}
      helperText={meta.error}
    />
  </div>
);

FinalTextField.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
};

export default FinalTextField;
