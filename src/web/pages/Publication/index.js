import React from 'react';
import { Chart } from '@sis-cc/dotstatsuite-components';
import { compose, lifecycle } from 'recompose';
import { Grid, Button } from '@material-ui/core';
import { connect } from 'react-redux';
import { publish, startPublication, endPublication, loadSharedChart } from '../../ducks/chart';
import { getSharedChartData, getChartError, getStatus, isDone } from '../../selectors/chart';
import { getCaptchaKey, getConfirmUrl, getChartId, getTenantId } from '../../selectors/config';
import { getInitialChartData } from '../../selectors/settings';
import { Header, HeaderLeft, HeaderRight } from '../../components/InnerHeader';
import { isPublishing, back, isSharedMode } from './utils';
import PublicationForm from '../../components/PublicationForm';

const mapStateToProps = state => ({
  intialChartData: getInitialChartData(state),
  sharedChartData: getSharedChartData(state),
  tenantId: getTenantId(state),
  chartId: getChartId(state),
  status: getStatus(state),
  confirmUrl: getConfirmUrl(state),
  chartError: getChartError(state),
  isDone: isDone(state),
  sitekey: getCaptchaKey(state),
});

const mapDispatchToProps = dispatch => ({
  publish: confirmUrl => data => email => dispatch(publish({ email, data, confirmUrl })),
  loadSharedChart: id => dispatch(loadSharedChart(id)),
  startPublication: () => dispatch(startPublication()),
  endPublication: () => dispatch(endPublication()),
  isSharedMode: chartId => isSharedMode(chartId),
});

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  publish: dispatchProps.publish(stateProps.confirmUrl),
});

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps,
  ),
  lifecycle({
    componentDidMount() {
      const { loadSharedChart, chartId } = this.props;
      if (isSharedMode(chartId)) loadSharedChart(chartId);
    },
  }),
);

const App = ({
  sharedChartData,
  intialChartData,
  chartId,
  tenantId,
  isSharedMode,
  startPublication,
  endPublication,
  publish,
  status,
  chartError,
  isDone,
  sitekey,
}) =>
  isSharedMode(chartId) ? (
    <Grid>
      <Grid item>
        <Header>
          <HeaderLeft>Shared Chart #{chartId}</HeaderLeft>
          <HeaderRight>
            <Button color="inherit" onClick={() => back(tenantId)}>
              Back
            </Button>
          </HeaderRight>
        </Header>
      </Grid>
      <Grid item>
        <Chart {...sharedChartData} />
      </Grid>
    </Grid>
  ) : (
    <Grid>
      <Grid item>
        <Header>
          <HeaderLeft>Original Chart</HeaderLeft>
          <HeaderRight>
            <Button color="inherit" onClick={() => startPublication()}>
              Share
            </Button>
          </HeaderRight>
        </Header>
      </Grid>
      <Grid item>
        <PublicationForm
          open={isPublishing(status)}
          onClick={publish(intialChartData)}
          endPublication={endPublication}
          error={chartError}
          isDone={isDone}
          sitekey={sitekey}
        />
        <Chart {...intialChartData} />
      </Grid>
    </Grid>
  );

export default enhance(App);
