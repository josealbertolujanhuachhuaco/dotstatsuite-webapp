import React from 'react';
import { Form, Field } from 'react-final-form';
import PropTypes from 'prop-types';
import { IconButton, Grid, Button, withStyles } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Close } from '@material-ui/icons';
import FinalTextField from './FinalTextField';
import Captcha from './Captcha';
import { showPublicationMsg } from './utils';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

const styles = {
  dialog: {
    position: 'absolute',
    margin: '60px auto',
    paddingTop: '0px',
    width: 500,
    height: 570,
  },
  textField: {
    width: 300,
    minHeight: 70,
    marginBottom: 30,
  },
  captcha: {
    width: 300,
    marginBottom: 30,
  },
  msg: {
    minHeight: 55,
    maxWidth: 320,
  },
  exitBtn: {
    position: 'absolute',
    top: 10,
    right: 10,
  },
  hr: {
    marginLeft: 25,
    marginRight: 25,
  },
};

const validate = values => {
  const errors = {};
  const email = /\S+@\S+\.\S+/;

  if (!email.test(values.email)) {
    errors.email = <FormattedMessage {...messages.emailErrorMsg} />;
  }
  if (!values.captcha) {
    errors.captcha = <FormattedMessage {...messages.captchaErrormsg} />;
  }
  return errors;
};

const PublicationForm = ({ open, onClick, endPublication, isDone, error, classes, sitekey }) => (
  <Dialog open={open} className={classes.dialog} maxWidth="sm" fullWidth>
    <DialogTitle>
      <FormattedMessage {...messages.dialogTitle} />
      <IconButton className={classes.exitBtn} onClick={endPublication}>
        <Close />
      </IconButton>
    </DialogTitle>
    <hr className={classes.hr} />
    <DialogContent>
      <Form
        onSubmit={({ email }) => onClick(email)}
        validate={validate}
        render={({ handleSubmit, valid }) => (
          <form onSubmit={handleSubmit}>
            <Grid container direction="column" justify="flex-end" alignItems="center">
              <Grid item className={classes.textField}>
                <Field
                  name="email"
                  type="email"
                  component={FinalTextField}
                  label={<FormattedMessage {...messages.email} />}
                />
              </Grid>

              <Grid item className={classes.captcha}>
                <Field name="captcha" sitekey={sitekey} component={Captcha} />
              </Grid>

              <Grid item className={classes.msg}>
                {showPublicationMsg(isDone, error)}
              </Grid>

              <Grid item container direction="row" justify="flex-end" alignItems="flex-end">
                <Grid item>
                  <Button color="primary" disabled={!isDone} onClick={endPublication}>
                    <FormattedMessage {...messages.done} style={{ fontSize: 'xx-small' }} />
                  </Button>
                </Grid>

                <Grid item>
                  <Button type="submit" disabled={!valid || isDone} color="primary">
                    <FormattedMessage {...messages.publish} />
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </form>
        )}
      />
    </DialogContent>
  </Dialog>
);

PublicationForm.propTypes = {
  open: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  endPublication: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  isDone: PropTypes.bool.isRequired,
  error: PropTypes.bool,
  sitekey: PropTypes.string,
};

export default withStyles(styles)(PublicationForm);
