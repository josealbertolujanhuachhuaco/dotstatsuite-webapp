import { createSelector } from 'reselect';
import { prop } from 'ramda';

export const root = prop('auth');
export const getUser = createSelector(
  root,
  prop('user'),
);
