import { mergeAll } from 'ramda';

const getInput = req => mergeAll([req.query, req.body]);
const parseUrl = url => {
  const re = new RegExp(/^\/+(\w+)\/*/);
  const [_, service] = re.exec(url); // eslint-disable-line no-unused-vars
  return service;
};

export const getMessage = (service, req) => {
  const method = parseUrl(req.path);
  const input = getInput(req);
  return { service, method, input };
};

const server = (service, evtx) => (req, res, next) => {
  if (!evtx) return next(new Error('EvtX is not configured!'));
  evtx
    .run(getMessage(service, req), { req })
    .then(data => res.json(data))
    .catch(next);
};

export default server;
