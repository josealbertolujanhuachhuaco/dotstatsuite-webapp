import { getSettings, getInitialChartData } from '../settings';

const state = {
  settings: {
    intialData: {},
  },
};

describe('selectors | settings', () => {
  it('getSettings', () => {
    const { settings } = state;
    expect(getSettings(state)).toEqual(settings);
  });

  it('getInitialChartData', () => {
    const {
      settings: { intialData },
    } = state;
    expect(getInitialChartData(state)).toEqual(intialData);
  });
});
