import debug from 'debug';
import evtX from 'evtx';
import initApi from './api';
import { initServices } from '../../utils';

const loginfo = debug('webapp:evtx:api');

const services = [initApi];

const init = ctx => {
  const api = evtX(ctx).configure(initServices(services));
  loginfo('evtX RENDER setup.');
  return Promise.resolve({ ...ctx, services: { ...ctx.services, api } });
};

export default init;
