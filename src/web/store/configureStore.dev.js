import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import reducer from '../ducks';

const configureStore = (initialState = {}) => {
  const logger = createLogger();
  const middlewares = [thunk, logger];
  return createStore(reducer, initialState, composeWithDevTools(applyMiddleware(...middlewares)));
};

export default configureStore;
