import configureStore from '../../store/configureStore.test';
import reducer, {
  PUBLISHING,
  PUBLISHED,
  START_CHART_PUBLICATION,
  CHART_PUBLISHED,
  CHART_ERROR,
  END_CHART_PUBLICATION,
  LOAD_CHART,
  publish,
} from '../chart';

describe('reducer | chart', () => {
  const chart = {
    status: PUBLISHED,
    hasError: false,
    publication: {},
    sharedChart: {},
  };

  it('should return the initial state', () => {
    expect(reducer(chart, {})).toEqual(chart);
  });

  it('should start publication', () => {
    const action = { type: START_CHART_PUBLICATION };
    expect(reducer(chart, action)).toEqual({ status: PUBLISHING });
  });

  it('should publish chart', () => {
    const action = { type: CHART_PUBLISHED, publication: { status: 'pending', email: 'test@mail.com' } };
    expect(reducer(chart, action)).toEqual({
      ...chart,
      publication: {
        status: 'pending',
        email: 'test@mail.com',
      },
    });
  });

  it('should return error', () => {
    const action = { type: CHART_ERROR };
    expect(reducer(chart, action)).toEqual({ ...chart, hasError: true });
  });

  it('should end publication', () => {
    const action = { type: END_CHART_PUBLICATION };
    expect(reducer(chart, action)).toEqual({ ...chart, status: PUBLISHED });
  });

  it('should load chart', () => {
    const action = { type: LOAD_CHART, data: { data: {}, type: {}, option: {} } };
    expect(reducer(chart, action)).toEqual({ sharedChart: { data: {}, type: {}, option: {} } });
  });
});

// jest.mock('../../../lib/share', () => ({
//   publish: () => Promise.resolve('pending'),
//   get: () => Promise.resolve({ data: 'data', type: 'type', option: 'option' }),
// }));

// describe('chart', () => {
//   it('should publish chart', done => {
//     const initialState = {};
//     const hook = {
//       [CHART_PUBLISHED]: getState => {
//         const {
//           chart: { publication },
//         } = getState();
//         expect(publication).toEqual('pending');
//         done();
//       },
//     };
//     const body = { email: 'test@com', data: {}, confirmUrl: 'test.com' };
//     const store = configureStore(initialState, hook);
//     store.dispatch(publish(body));
//   });
// });
