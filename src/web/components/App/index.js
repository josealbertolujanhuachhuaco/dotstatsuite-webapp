import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route, Switch } from 'react-router-dom';
import { map } from 'ramda';
import { Grid } from '@material-ui/core';
import Header from '../Header';
import withRoutes from '../../hoc/routes';
import Auth from '../Auth';

const defaultAppRoute = routes => {
  const route = routes.getDefault();
  if (route) return <Redirect to={route.path} />;
};

const withAuth = ({ isAuthRequired }, route) => props =>
  isAuthRequired(route) ? (
    <Auth route={route}>
      <route.component {...props} />
    </Auth>
  ) : (
    <route.component {...props} />
  );

const App = ({ routes }) => (
  <Grid>
    <Grid item>
      <Header />
    </Grid>
    <Grid item>
      <Switch>
        {map(
          route => (
            <Route key={route.path} exact={route.exact} path={route.path} render={withAuth(routes, route)} />
          ),
          routes.getRoutes(),
        )}
        {defaultAppRoute(routes)}
      </Switch>
    </Grid>
  </Grid>
);

App.propTypes = {
  routes: PropTypes.object.isRequired,
};

export default withRoutes(App);
