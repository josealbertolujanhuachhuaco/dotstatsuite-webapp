import { PUBLISHING } from '../../ducks/chart';
import { isNil } from 'ramda';

export const isPublishing = status => status === PUBLISHING;

export const isSharedMode = chartId => !isNil(chartId);

export const back = tenantId => (window.location = `/?tenant=${tenantId}`);
