import axios from 'axios';
import shareAPI, { getPublishUrl, getChartUrl } from '../api/share';

jest.mock('axios');

beforeEach(() => {
  jest.clearAllMocks();
});

describe('share | url', () => {
  it('should return publish url', () => {
    const url = 'localhost/api/charts';
    expect(getPublishUrl('localhost')).toEqual(url);
  });

  it('should return chart url', () => {
    const url = 'localhost/api/charts/1';
    expect(getChartUrl('localhost')(1)).toEqual(url);
  });
});

describe('share | axios', () => {
  const ctx = { globalConfig: { shareServerUrl: 'shareServerUrl' } };
  const { publish, get } = shareAPI(ctx);

  it('should publish a chart at the right endpoint', () => {
    axios.post.mockResolvedValue();
    const expectedPublishUrl = `${ctx.globalConfig.shareServerUrl}/api/charts`;
    return publish({ chart: 'chart' }).then(() => {
      expect(axios.post).toHaveBeenCalledTimes(1);
      expect(axios.post).toHaveBeenCalledWith(expectedPublishUrl, 'chart');
    });
  });

  it('should fetch a chart at the right endpoint', () => {
    axios.get.mockResolvedValue();
    const id = { id: 'id' };
    const expectedFetchUrl = `${ctx.globalConfig.shareServerUrl}/api/charts/${id.id}`;
    return get({ id: 'id' }).then(() => {
      expect(axios.get).toHaveBeenCalledTimes(1);
      expect(axios.get).toHaveBeenCalledWith(expectedFetchUrl);
    });
  });
});
